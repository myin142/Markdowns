
---
### Headers
---

# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

---
### Style
---

*Emphasis*

_Underline_

**Strong**

__Strong2__

~~Strike~~

__Combined Bold and *emphasis*__

---
### Lists
---

1. Item
 * Sub Item
 - Sub Item
 + Sub Item
2. Item
 1. Sub Item
 - Sub Item
 4. Sub Item 
9. Item

---
### Links
---

[Link](https://google.com)

[Link with Title](https://google.com "Google")

[Link with Variable][Variable]

[Variable]

[Local File](./README.md)

[Variable]: http://youtube.com

![Alt Text](file_path)

---
### Code
---

`code`

```javascript

var hello = "Hello World";

```

---
### Table
---

| Tables | Col 1 | Col 2 |
|:------:|:------|------:|
| *Row 1* |       |       |
|**Row 2**|       |       |

---
### HTML
---

<em>HTML in <strong>Markdown</strong></em>

---
### Underline
---

---
***
___
