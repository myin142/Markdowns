# Summary of [Tae Kim's Grammar Guide](http://www.guidetojapanese.org/learn/grammar/)

## Particles
 * Basics
   * は - Topic
   * も - Inclusive Topic
   * が - Identifier
   * を - Object
   * に - Target
   * へ - Direction
   * で - Contextual
   * と - Inclusive
   * や・とか - Vague Listing
   * の - Possession, Noun Replacement, Explanation
	  * の - Possesion/Replacement -> な not replaced
	  * (な)のだ -> (な)んだ - な when Noun/Na-Adj
   * ね - Agreement
   * よ - Informing
   * か - Question
   * さ - casual よ
     * わ - feminine よ
     * ぞ / ぜ - manly よ
   * な - rough ね
     * かしら - feminine かな
   * かい - yes/no questions
   * だい - open-ended questions
 * Compound
   * (だ)から - Reason
   * (な)ので・(な)んで - 2 Sentence with Explanatory Tone
   * のに - Despite
   * (だ)が・(だ)けど - Contradiction
   * (だ)し - Multiple Reasons
   * ~たりする - Multiple Actions or States
 * Te-Forms
   * ~て(い)る - Enduring State -> RU-Verb
   * ~てある - Resultant State
   * ~ておく/~とく - Preparation for future
   * ~て(も) いい / 大丈夫 - Something is (not) ok
   * ~て 欲しい - Want something done
 * Conditions
   * [Condition] + (だ)と + [Result]
   * [Assumed Context] + なら(ば) + [Result]
   * もし - Uncertain Condition
 * Quotes / Action Clauses
   * Direct: と + Verb -> って
   * Indirect: (だ)と + Verb -> (だ)って
   * という - define, describe, rephrasing, conclusions -> (っ)て
 * Trying
   * Trying: ~て + みる
   * Attempting: Volation + とする
 * Favors
   * Giving (Speaker POV): ~て + あげる
   * Giving (Receiver POV): ~て + くれる
   * Receive: ~て + もらう
   * Asking Favour: ~て + くれる / もらえる
   * Asking not to do: ~ない + で + くれる / ~(もらえる)~
 * Requests
   * Polite: ~て + (ください) / くれ
   * Neg. Polite: ~なくて + で + (ください) / くれ
   * Casual: ~て + ちょうだい
   * Neg. Polite: ~なくて + で + ちょうだい
   * Firm but Polite: Stem + な(さい)
 * Casual / Slangs
   * じゃん - confirm the positive
   * つ - harder, hissing sound という
   * ってば / ったら - exasperation
   * なんか - "Like", Filler Word
   * Stem + やがる -> contempt, hatred for action

## Na-Adjective
|Conjugation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal|静か|静かじゃない|静かだった|静かじゃなかった|
|Adverb|静かに||||
|Polite|静かです|静かじゃないです|静かでした|静かじゃなかったです|
|Compound|静かで|静かじゃなくて|||
|Conditional|静かであれば|静かじゃなければ|静かだったら(ば)|静かじゃなかったら(ば)|

|Explanation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal||+ じゃない|+ だった|+ じゃなかった|
|Adverb|+ に||||
|Polite|+ です|+ です|+ でした|+ です|
|Compound|+ で|な~~い~~ -> くて|||
|Conditional|+ であれば|な~~い~~ -> ければ|Past + ら(ば)|Past + ら(ば)|

## I-Adjective
|Conjugation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal|高い|高くない|高かった|高くなかった|
|Adverb|高く||||
|Polite|高いです|高くないです|高かったです|高くなかったです|
|Compound|高くて|高くなくて|||
|Conditional|高ければ|高くなければ|高かったら(ば)|高くなかったら(ば)|

|Explanation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal||~~い~~ -> くない|~~い~~ -> かった|な~~い~~ ->　くなかった|
|Adverb|~~い~~ -> く||||
|Polite|+ です|+ です|+ です|+ です|
|Compound|~~い~~ -> くて|な~~い~~ -> くて|||
|Conditional|~~い~~ -> ければ|な~~い~~ -> ければ|Past + ら(ば)|Past + ら(ば)|

## U-Verb
|Conjugation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal|待つ|待たない|待った|待たなかった|
|Polite|待ちます|待ちません|待ちました|待ちませんでした|
|Compound|待って|待たなくて|||
|Potential|待てる|待てない|待てた|待てなかった|
|Conditional|待てば|待たなければ|待ったら(ば)|待たなかったら(ば)|
|Must|待たなくてはだめ|待ってはだめ|||
|Want|待ちたい|待ちたくない|待ちたかった|待ちたくなかった|
|Volation|待とう||||
|Polite Volation|待ちましょう||||
|Command|待て|待つな|||

|Explanation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal||~あ + ない<br> う -> ~わ + ない|す -> した <br> く -> いた <br> ぐ -> いだ <br> む・ぶ・ぬ -> んだ <br> る・う・つ -> った|な~~い~~ -> なかった|
|Polite|~い + ます|~い + ません|~い + ました|~い + ませんでした|
|Compound|~~た~~ -> て|な~~い~~ -> くて|||
|Potential|~え + る|RU-Verb|RU-Verb|RU-Verb|
|Conditional|~え + ば|な~~い~~ -> ければ|Past + ら(ば)|Past + ら(ば)|
|Must|~なくて + は + X<br>(なくて -> なくちゃ)<br>~ない + と + X<br>~なければ + X<br>(なければ -> なきゃ)|~て + は + X<br>(ては/では -> ちゃ/じゃ)|*X = だめ / いけない / ならない*||
|Want|~い + たい|I-Adj|I-Adj|I-Adj|
|Volation|~お + う||||
|Polite Volation|~い + ましょう||||
|Command|~え|+ な|||

## RU-Verb
|Conjugation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal|食べる|食べない|食べた|食べなかった|
|Polite|食べます|食べません|食べました|食べませんでした|
|Compound|食べて|食べなくて|||
|Potential|食べ(ら)れる|食べ(ら)れない|食べ(ら)れた|食べ(ら)れなかった|
|Conditional|食べれば|食べなければ|食べたら(ば)|食べなかったら(ば)|
|Must|食べなくてはだめ|食べてはだめ|||
|Want|食べたい|食べたくない|食べたかった|食べたくなかった|
|Volation|食べよう||||
|Polite Volation|食べましょう||||
|Command|食べろ|食べるな|||

|Explanation|Non-Past|Negative|Past|Neg. Past|
|:-:|:-:|:-:|:-:|:-:|
|Normal||~~る~~ -> ない|~~る~~ -> た|な~~い~~ -> なかった|
|Polite|~~る~~ -> ます|~~る~~ -> ません|~~る~~ -> ました|~~る~~ -> ませんでして|
|Compound|~~た~~ -> て|な~~い~~ -> くて|||
|Potential|~~る~~ -> (ら)れる|RU-Verb|RU-Verb|RU-Verb|
|Conditional|~え + ば|な~~い~~ -> ければ|Past + ら(ば)|Past + ら(ば)|
|Must|~なくて + は + X<br>(なくて -> なくちゃ)<br>~ない + と + X<br>~なければ + X<br>(なければ -> なきゃ)|~て + は + X<br>(ては/では -> ちゃ/じゃ)|*X = だめ / いけない / ならない*||
|Want|~~る~~ + たい|I-Adj|I-Adj|I-Adj|
|Volation|~~る~~ -> よう||||
|Polite Volation|~~る~~ -> ましょう||||
|Command|~~る~~ -> ろ|+ な|||

## Exceptions
||する|くる|ある|行く|くれる|
|:-:|:-:|:-:|:-:|:-:|:-:|
|Neg.|しない|こない|ない|||
|Past|した|きった||行った||
|Polite|します|きます||||
|Potential|できる|こられる||||
|Command|しろ|こい|||くれ|

Polite: ないです -> ありません / ありませんでした
