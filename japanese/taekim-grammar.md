<style>
.color{
	color: orange;
}
</style>

# [Tae Kim's Grammar Guide](http://www.guidetojapanese.org/learn/grammar/)

## Basic Grammar

#### State of Being
|		 |Positive	|Negative  	|
|:------:|:--------:|:---------:|
|Non-Past|だ   		|じゃない    	|
|Past    |だった		|じゃなかった	|

#### Particles
 * は - Topic
 * も - Inclusive Topic
 * が - Identifier
 * を - Object
 * に - Target
 * へ - Direction
 * で - Contextual
 * と - Inclusive
 * や・とか - Vague Listing
 * の -  Possession, Noun Replacement, Explanation
	* の - Possesion/Replacement -> な not replaced
	* (な)のだ -> (な)んだ - な when Noun/Na-Adj
 * ね - Agreement
 * よ - Informing

#### Na-Adjectives

[Adj] + な + [Noun]

|		 |Positive	|Negative  	|
|:------:|:--------:|:---------:|
|Non-Past|な        	|じゃない  	|
|Past    |だった     	|じゃなかった  	|

#### I-Adjectives

No だ.

|		 |Positive							|Negative  								|
|:------:|:--------------------------------:|:-------------------------------------:|
|Non-Past|高<span class="color">い</span> 	|高<span class="color">くない</span> 		|
|Past    |高<span class="color">かった</span> |高<span class="color">くなかった</span>	|

#### Ru-Verbs
iru/eru ending

|		 |Positive							|Negative  								|
|:------:|:--------------------------------:|:-------------------------------------:|
|Non-Past|食べる	       						|食べ<span class="color">ない</span> 		|
|Past    |食べ<span class="color">た</span>	|食べ<span class="color">なかった</span>  	|

#### U-Verbs

|		 |Positive							|Negative 								|
|:------:|:--------------------------------:|:-------------------------------------:|
|Non-Past|待つ               				|待<span class="color">たない</span> 		|
|Past    |待<span class="color">った</span>  	|待<span class="color">たなかった</span>	|

|Ending	 	|Past	  	|
|:---------:|:---------:|
|す		    |した     	|
|く		    |いた		|
|ぐ		    |いだ	  	|
|む・ぶ・ぬ		|んだ	  	|
|る・う・つ  	|った	       	|

|Exception	|Negative	|Past	|Past Negative	|
|:---------:|:---------:|:-----:|:-------------:|
|する		|しない		|した   	|しなかった		|
|くる		   	|こない		|きった	|こなかった	    |
|ある		|ない		|		|			    |
|行く	   	|			|行った	|			    |

#### Transitive & Intransitive
Action done by active agent & without direct agent. <br />
Intransitive should not be used with を. (Except when location is object)

#### Adverb

|		|Adjective						|Adverb    							|
|:-----:|:-----------------------------:|:---------------------------------:|
|I-Adj	|早<span class="color">い</span>	|早<span class="color">く</span>		|
|Na-Adj	|きれい             				|きれい<span class="color">に</span>	|

## Essential Grammar

#### Polite Form

|		|Verb	|Polite  							|
|:-----:|:-----:|:---------------------------------:|
|Ru-Verb|食べる	|食べ<span class="color">ます</span> 	|
|U-Verb |泳ぐ	|泳<span class="color">ぎます</span> 	|
|		|する	|します								|
|	    |くる	    |きます								|

Stem: Verb -ます

|I-Adj		|Casual			|Polite										|
|:---------:|:-------------:|:-----------------------------------------:|
|Plain		|かわいい			|かわいい<span class="color">です</span>		|
|Negative	|かわいくない		|かわいくない<span class="color">です</span>	|
|Past		|かわいかった		|かわいかった<span class="color">です</span>	|
|Past-Neg	|かわいくなかった	|かわいくなかった<span class="color">です</span>	|

|Na-Adj		|Casual			|Polite										|
|:---------:|:-------------:|:-----------------------------------------:|
|Plain		|静か（だ）		|静か<span class="color">です</span>			|
|Negative	|静かじゃない		|静かじゃない<span class="color">です</span>	|
|Past		|静かだった		|静か<span class="color">でした</span>		|
|Past-Neg	|静かじゃなかった	|静かじゃなかった<span class="color">です</span>	|

"Official" Conjugation: ないです　－＞　ありません

#### Particles
 * か - Question
 * (だ)から - Reason
 * (な)ので・(な)んで - 2 Sentence with Explanatory Tone
 * のに - Despite
 * (だ)が・(だ)けど - Contradiction
 * (だ)し - Multiple Reasons
 * ~たりする - Multiple Actions or States
 * さ - casual よ, "Like"
   * わ - feminine よ
   * ぞ / ぜ - "cool" and manly よ
 * な - rough version of ね
   * かしら - feminine version of かな
 * Masculing sentence ending for questions
   * かい - yes/no questions
   * だい - open-ended questions

#### Question Words

|Word	|Meaning  				|
|:-----:|:---------------------:|
|誰か	|Someone				|
|何か	|Something				|
|いつか	|Sometime				|
|どこか	|Somewhere				|
|どれか	|A certain one from many|
|誰も	|Everybody/Nobody		|
|何も	|Nothing				|
|いつも	|Always					|
|どこも	|Everywhere				|
|どれも	|Any and all			|
|誰でも	|Anybody				|
|何でも	|Anything				|
|いつでも	|Anytime				|
|どこでも	|Anywhere				|
|どれでも	|Whichever				|

#### Compound Sentences

|Type			|Example  							|
|:-------------:|:---------------------------------:|
|Noun, Na-Adj	|静か<span class="color">で</span>	|
|Neg. Noun/Na-Adj/Verb, I-Adj	|高<span class="color">くて</span>	|
|Verb|学生でし<span class="color">て</span>|

#### Te-Form
 * ~て(い)る - Enduring State -> RU-Verb
 * ~てある - Resultant State
 * ~ておく/~とく - Preparation for future

#### Potential Form
||Verb|Potential|
|:-:|:-:|:-:|
|Ru-Verb|見る|見<span class="color">(ら)れる</span>|
|U-Verb|遊ぶ|遊<span class="color">べる</span>|
||する|できる|
||くる|こられる|
||ある|あり得る|

-> RU-Verb

ありうる/ありえる

#### Suru/Naru
Noun/Na-Adj: + に なる/する
I-Adj: Adverb + に なる
Verbs: + こと/よう なる/する

#### Conditionals
 * [Condition] + (だ)と + [Result]
 * [Assumed Context] + なら(ば) + [Result]
 * Conjugation for ば - focus on condition

||Normal|Conditional|
|:-:|:-:|:-:|
|U-Verb|待つ|待<span class="color">てば</span>|
|Ru-Verb|食べる|食べ<span class="color">れば</span>|
|I-Adj|おかしい|おかし<span class="color">ければ</span>|
|Neg Ending|ない|な<span class="color">ければ</span>|
|Noun/Na-Adj|学生|学生<span class="color">であれば</span>|
 * ~たら(ば) - focus on result
 * もし - uncertain if condition is true

#### Must / Have to
 * Must not: [Te-Form] + は + だめ / いけない / ならない
 * Must:
  * [Negative Te-Form] + は + だめ / いけない / ならない
  * [Negative Verb] + と Conditional + だめ / いけない / ならない
  * [Negative Verb] + ば Conditional + だめ / いけない / ならない
 * Abbreviations (Casual) without だめ / いけない / ならない
  * なくて -> なくちゃ
  * なければ -> なきゃ
  * と without rest
  * ては/では -> ちゃ/じゃ + need rest
 * Something is ok
  * ~て(も) いい/大丈夫

#### Desire and Suggestion
 * Stem + ~たい -> I-Adj
 * 欲しい - Verb in Te-Form
 * Volation Form

||Verb|Volation|
|:-:|:-:|:-:|
|Ru-Verb|食べる|食べよう|
|U-Verb|入る|入ろう|

 * Polite Volation

||Verb|Volation|
|:-:|:-:|:-:|
|Ru-Verb|食べる|食べましょう|
|U-Verb|入る|入りましょう|

 * Suggestions: ば / たら condition + どう

#### Action Clause
 * Direct Quote: と + Verb
 * Indirect Quote: (だ)と + Verb
 * って = casual と = は

#### Define and Describe
 * と いう - Define
 * と/こう/そう/ああ/どう いう - In relative clause to describe anything
 * という か/こと - Making Conclusion, Rephrasing -> correct sth., different conclusions, こと - sum up sth.
 * という -> (っ)て - only when at beginning

#### Trying and Attempting
 * Trying: Te-Form + みる
 * Attempting: Volation + とする

#### Giving and Receiving
 * Giving (Speaker's Point of View): Te-Form + あげる
   * Giving (Pets, Animals): Te-Form + やる
 * Giving (Receiver's Point of View): Te-Form + くれる
 * Receive: Te-Form + もらう
 * Asking Favour: Te-Form + くれる / もらえる
   * Negative Form to make softer
 * Asking not to do: Neg. + で + くれる / (もらえる)~

#### Requests
 * Polite - Drop ください in casual speech
   * Te-Form + ください / くれ (More commanding)
   * Negative + で + ください / くれ
 * Casual - ちょうだい -> like Polite version, slightly feminine and childish
 * Firm but Polite - Drop さい for Casual
   * Stem + なさい -> only positive
 * Command Form:

||Verb|Command|
|:-:|:-:|:-:|
|Ru-Verb|食べる|食べろ|
|U-Verb|入る|入れ|
||する|しろ|
||くる|こい|
||くれる|くれ|

 * Negative Command Form: Verb + な

#### [Numbers and Counting](http://www.guidetojapanese.org/learn/grammar/numbers)

#### Casual / Slang
 * じゃない -> じゃん - But used for confirm the positive
 * という -> つ - harder,hissing sound
 * といえば / といったら -> ってば / ったら - show exasperation
 * なにか -> なんか - "Like", Filler Word
 * Stem + やがる -> showing contempt, hatred for action
