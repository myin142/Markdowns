# [Tae Kim's Grammar Guide Vocabulary](http://www.guidetojapanese.org/learn/grammar/)

## Basic Grammar

|Kanji	 	|Reading 	|Type		|Translation			|
|:---------:|:---------:|:---------:|:---------------------:|
|人      	|ひと			|Noun		|Person    				|
|学生    	|がくせい    	|Noun		|Student  				|
|元気    	|げんき	    |Na-Adj		|Healthy, Lively		|
|友達    	|ともだち     	|Na-Adj		|Friend		 			|
|明日    	|あした		|Noun		|Tomorrow   			|
|今日    	|きょう	    |Noun		|Today		  			|
|試験    	|しけん	    |Noun		|Exam		  			|
|でも     	|でも	    |Particle	|But		  			|
|誰	     	|だれ	    |Noun		|Who					|
|私		 	|わたし	    |Noun		|Me, Myself, I			|
|静か     	|しずか	    |Na-Adj		|Quiet					|
|きれい	    |きれい	    |Na-Adj		|Pretty, Clean			|
|親切	 	|しんせつ     |Na-Adj		|Kind					|
|魚		 	|さかな	    |Noun		|Fish					|
|好き	    |すき	    |Na-Adj		|Likeable, Desirable	|
|肉		 	|にく	       	|Noun		|Meat					|
|野菜	 	|やさい	    |Noun		|Vegetables				|
|嫌い	    |きらい		|Na-Adj		|Hateful, Distasteful	|
|食べ物	 	|たべもの  	|Noun		|Food					|
|おいしい	    |おいしい  	|I-Adj		|Tasty	 				|
|高い	    |たかい	    |I-Adj		|High, Tall, Expensive	|
|値段	 	|ねだん	    |Noun		|Price					|
|いい	    |いい	    |I-Adj		|Good					|
|彼		 	|かれ	    |Noun		|He, Boyfriend			|
|かっこいい 	|かっこいい 	|I-Adj		|Cool, Handsome			|
|食べる	   	|たべる	    |Ru-Verb	|to eat					|
|分かる	   	|わかる	    |U-Verb		|to understand			|
|見る	   	|みる	    |Ru-Verb	|to see					|
|寝る	   	|ねる	    |Ru-Verb	|to sleep				|
|起きる	   	|おきる	    |Ru-Verb	|to wake, to occur		|
|考える	   	|かんがえる  	|Ru-Verb	|to think				|
|教える	   	|おしえる		|Ru-Verb	|to teach, to inform	|
|出る	   	|でる	    |Ru-Verb	|to come out			|
|いる	   	|いる	    |Ru-Verb	|to exist (animate)		|
|着る	   	|きる	    |Ru-Verb	|to wear				|
|話す	   	|はなす	    |U-Verb		|to speak				|
|聞く	   	|きく	       	|U-Verb		|to ask, to listen		|
|泳ぐ	   	|およぐ	    |U-Verb		|to swim				|
|遊ぶ	   	|あそぶ	    |U-Verb		|to play				|
|待つ	   	|まつ	    |U-Verb		|to wait				|
|飲む	   	|のむ	    |U-Verb		|to drink				|
|買う	   	|かう	       	|U-Verb		|to buy					|
|ある	   	|ある	    |U-Verb		|to exist (inanimate)	|
|死ぬ	   	|しぬ	    |U-Verb		|to die					|
|する		|する	    |Exc-Verb	|to do					|
|来る	   	|くる	       	|Exc-Verb	|to come				|
|お金		|おかね	    |Noun		|Money					|
|猫		 	|ねこ	    |Noun		|Cat					|
|要る	    |いる	    |U-Verb		|to need				|
|帰る	    |かえる	    |U-Verb		|to go home				|
|切る	    |きる	    |U-Verb		|to cut					|